

#include <stdio.h>

#include <sys/types.h>	//	open
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>		// close

#include <string.h>		//	memset

#include <stdlib.h>		//	atoi

int main_1(int argc, char **argv)
{
	char *file = NULL;
	int fd1;

	if(argc > 1){
		file = argv[1];
	} else {
		file = (char *)"/dev/cd_sample0";
	}

	int fd = open(file, O_RDWR);

	if(fd < 0){
		perror("Open!\n");
		return -1;
	}

	close(fd);


	fd = open(file, O_RDONLY);

	if(fd < 0){
		perror("Open!\n");
		return -1;
	}

	fd1 = open(file, O_WRONLY);
	if(fd1 < 0){
		perror("Open!\n");
		return -1;
	}

	sleep(20);

	close(fd1);

	close(fd);
	

	
	return 0;
}


int main(int argc, char **argv)
{
	char *file = NULL;
	int ret;
	char buf[32];
	int pid;
	int fd;

	file = (char *)"/dev/cd_sample0";

	if(argc > 1){
		pid = atoi(argv[1]);
	} else {
		printf("Input pid !");
		return -1;
	}

	fd = open(file, O_RDWR);

	if(fd < 0){
		perror("Open!\n");
		return -1;
	}

	memset(buf, 0x0, sizeof(buf));
	sprintf(buf, "%d,%d\n", pid, 0x55);
	ret = write(fd, buf, sizeof(buf));
	if(ret < sizeof(buf)){
		perror("Write!\n");
		return -2;
	}

	close(fd);

	return 0;
}






